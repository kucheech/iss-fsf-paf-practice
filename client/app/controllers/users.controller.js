(function () {
    "use strict";
    angular.module("MyApp").controller("UsersCtrl", UsersCtrl);

    UsersCtrl.$inject = ["MyAppService", "$state"];

    function UsersCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.users = [];

        vm.editUser = function (id) {
            // console.log(id);
            $state.go("edit", { id: id });
        }

        vm.init = function () {
            MyAppService.getUsers()
                .then(function (result) {
                    // console.log(result);
                    vm.users = result;
                }).catch(function (err) {
                    console.log(err);
                });
        }

        vm.init();


    }

})();