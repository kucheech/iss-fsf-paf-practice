(function () {
    "use strict";
    angular.module("MyApp").controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["MyAppService", "$state"];

    function EditCtrl(MyAppService, $state) {
        var vm = this; // vm
        vm.account = null;
        vm.id = 0;

        vm.init = function () {
            // console.log($state.params.id);
            vm.id = $state.params.id;
            // vm.showResults = false;

            MyAppService.getAccountById(vm.id)
                .then(function (result) {
                    console.log(result);
                    vm.account = result;
                }).catch(function (err) {
                    console.log(err);
                });

        }

        vm.init();

        vm.cancelEdit = function() {
            $state.go("accounts");
        }

        vm.saveEdit = function() {
            MyAppService.updateAccount(vm.account)
            .then(function (result) {
                // console.log(result);
                $state.go("accounts");
            }).catch(function (err) {
                console.log(err)
            });
        }

    }

})();