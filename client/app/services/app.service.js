(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q", "$httpParamSerializerJQLike", "Upload"];

    function MyAppService($http, $q, $httpParamSerializerJQLike, Upload) {
        var service = this;

        //expose the following services
        service.getAccounts = getAccounts;
        service.getAccountById = getAccountById;
        service.updateAccount = updateAccount;
        service.login = login;
        service.logout = logout;
        service.getProtectedAccounts = getProtectedAccounts;
        service.resetPassword = resetPassword;
        service.createAccount = createAccount;
        service.createPost = createPost;
        service.getPosts = getPosts;
        service.uploadImage = uploadImage;
        service.submitPost = submitPost;

        function submitPost(post, file) {
            var defer = $q.defer();
            if (file != "") {
                uploadImage(file).then(function (result) {
                    // console.log(result);
                    post.image = result;
                    defer.resolve(createPost(post));
                }).catch(function (err) {
                    console.log(err)
                });
            } else {
                defer.resolve(createPost(post));
            }

            return defer.promise;
        }

        function uploadImage(file) {
            // console.log("uploadImage");
            var defer = $q.defer();
            Upload.upload({
                url: "/protected/upload",
                data: {
                    "img-file": file
                }
            }).then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function createPost(post) {
            var defer = $q.defer();

            $http.post("/protected/posts", { post: post })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function getPosts() {
            var defer = $q.defer();

            $http.get("/protected/posts").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function resetPassword(email) {
            var defer = $q.defer();
            $http.post("/resetpassword", { email: email })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getProtectedAccounts() {
            var defer = $q.defer();

            $http.get("/protected/accounts").then(function (result) {
                // console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }


        function logout() {
            var defer = $q.defer();

            $http.get("/logout").then(function (result) {
                console.log(result);
                defer.resolve(result);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function login(user) {
            // console.log("service login: " + user);
            var defer = $q.defer();
            $http.post("/login", user)
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function createAccount(account) {
            var defer = $q.defer();

            $http.post("/accounts", { account: account })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }


        function updateAccount(account) {
            var defer = $q.defer();
            const id = user.id;
            $http.post("/accounts/" + id, { account: account })
                .then(function (result) {
                    // console.log(result);
                    defer.resolve(result);
                }).catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getAccountById(id) {
            var defer = $q.defer();

            $http.get("/accounts/" + id).then(function (result) {
                // console.log(result);
                if (result.status == 200) {
                    defer.resolve(result.data);
                } else {
                    defer.resolve(null);
                }

            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

        function getAccounts() {
            var defer = $q.defer();

            $http.get("/accounts").then(function (result) {
                console.log(result);
                defer.resolve(result.data);
            }).catch(function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        }

    }

})();