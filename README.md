### server unit testing
1. ```npm install --save-dev babel-cli babel-preset-env jest supertest superagent```
1. change test in package.json to jest
1. ```npm test```

### Kill node window process
http://www.wisdomofjim.com/blog/how-kill-running-nodejs-processes-in-windows
```taskkill /im node.exe /F```

### auth local
```npm install passport passport-local body-parser express-session connect-ensure-login --save```

### HTTP Auth Interceptor Module
https://github.com/witoldsz/angular-http-auth
```bower install --save angular-http-auth```